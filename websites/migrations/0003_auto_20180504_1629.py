# Generated by Django 2.0.5 on 2018-05-04 14:29

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('websites', '0002_auto_20180504_1625'),
    ]

    operations = [
        migrations.AlterField(
            model_name='webpage',
            name='date_updated',
            field=models.DateTimeField(blank=True),
        ),
        migrations.AlterField(
            model_name='website',
            name='date_updated',
            field=models.DateTimeField(blank=True),
        ),
        migrations.AlterField(
            model_name='websitecategory',
            name='date_updated',
            field=models.DateTimeField(blank=True),
        ),
    ]
