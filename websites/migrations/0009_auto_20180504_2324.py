# Generated by Django 2.0 on 2018-05-04 21:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('websites', '0008_auto_20180504_2113'),
    ]

    operations = [
        migrations.AlterField(
            model_name='webpage',
            name='date_updated',
            field=models.DateTimeField(),
        ),
        migrations.AlterField(
            model_name='website',
            name='date_updated',
            field=models.DateTimeField(),
        ),
        migrations.AlterField(
            model_name='websitecategory',
            name='date_updated',
            field=models.DateTimeField(),
        ),
    ]
