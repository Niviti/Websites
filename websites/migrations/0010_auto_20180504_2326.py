# Generated by Django 2.0 on 2018-05-04 21:26

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('websites', '0009_auto_20180504_2324'),
    ]

    operations = [
        migrations.AlterField(
            model_name='webpage',
            name='date_updated',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='website',
            name='date_updated',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='websitecategory',
            name='date_updated',
            field=models.DateTimeField(blank=True, null=True),
        ),
    ]
