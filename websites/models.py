
from datetime import datetime
from django.db import models
# Create your models here.

class WebsiteCategory(models.Model):
       name = models.CharField(max_length=30)
       description = models.CharField(max_length=30)
       date_added= models.DateTimeField(default=datetime.now)
       date_updated= models.DateTimeField(null=True, blank=True)
       count= models.IntegerField()

       def __str__(self):
               return self.name

class Website(models.Model):
       category = models.ForeignKey(WebsiteCategory, on_delete=models.CASCADE)
       url = models.URLField()
       title2 = models.CharField(max_length=30)
       meta_description = models.CharField(max_length=200)
       alexa_rank =  models.IntegerField()
       date_added = models.DateTimeField(default=datetime.now)
       date_updated = models.DateTimeField(null=True, blank=True)


       def __str__(self):
               return self.title2
       

class WebPage(models.Model):
    website =  models.ForeignKey(Website, on_delete=models.CASCADE)
    url = models.URLField()
    date_added = models.DateTimeField(default=datetime.now)
    date_updated = models.DateTimeField(null=True, blank=True)
    title = models.CharField(max_length=30)
    meta_description = models.CharField(max_length=200)

    def __str__(self):
          return self.url