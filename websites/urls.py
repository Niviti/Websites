"""homeapp URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from .views import WebsiteListView
from django.urls import path, re_path
from django.conf.urls import url
# TEMPLATE PATHS
app_name = 'websites'

urlpatterns = [
   path('listview/all', WebsiteListView.as_view(), name='listview'),
   path('listview/<str:category>', WebsiteListView.as_view(), ),
   path('listview/<str:category>/<str:sort>', WebsiteListView.as_view(), name='sortlist'),
]
