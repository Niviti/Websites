from django.shortcuts import render

from django.shortcuts import render
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView

from .models import Website, WebsiteCategory



class WebsiteListView(ListView):
      model =  Website
      template_name = "websites_list.html"

      def get(self, request, *args, **kwargs):
            categories = WebsiteCategory.objects.all()
            object_list = Website.objects.all()
            default_value="None"

            kwarg_sort = kwargs.get('sort', default_value)
            kwarg_category = kwargs.get('category', default_value)

            if(kwarg_category=="all" and kwarg_sort!="None"):
                  object_list = Website.objects.all().order_by(kwarg_sort)
            if(kwarg_category!="None" and kwarg_sort=="None" and kwarg_category!="all"):
                category_value = WebsiteCategory.objects.get(name=kwarg_category)
                object_list = Website.objects.filter(category=category_value)
            if (kwarg_category!="None" and kwarg_sort!="None" and kwarg_category!="all"):
                   category_value = WebsiteCategory.objects.get(name=kwarg_category)
                   object_list =  Website.objects.filter(category=category_value).order_by(kwarg_sort)


            context = {
            'categories': categories,
            'object_list': object_list
            }

            return render(request, self.template_name, context)

